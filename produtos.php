<?
$h1         = 'Produtos';
$title      = 'Produtos';
$desc       = 'Encontre tudo para supermercado das melhores empresas. Receba diversos comparativos de preços pelo formulário com mais de 200 fornecedores. É grátis!';
$key        = 'produtos, materiais, equipamentos';
$var        = 'produtos';
include('inc/head.php');
?>
</head>
<body>

<? include('inc/topo.php');?>
<div class="wrapper">
 <main>
    <div class="content">
      <div id="breadcrumb" itemscope itemtype="http://schema.org/breadcrumb" >
        <a rel="home" itemprop="url" href="<?=$url?>" title="home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> home</span></a> »
        <strong><span class="page" itemprop="title">Produtos</span></strong>
      </div>
      <h1>Produtos</h1>   
      <article class="full">   
        <p>Encontre diversos produtos de aço das melhores empresas, para suas necessidades. Receba diversos comparativos pelo formulário com mais de 200 fornecedores.</p>
        <ul class="thumbnails-main">

          <li>
            <a rel="nofollow" href="<?=$url?>carrinhos-categoria" title="Carrinhos"><img src="imagens/produtos/carrinhos-01.jpg" alt="Carrinhos" title="Carrinhos"/></a>
            <h2><a href="<?=$url?>carrinhos-categoria" title="Carrinhos">Carrinhos</a></h2>
          </li>

          <li>
            <a rel="nofollow" href="<?=$url?>equipamentos-categoria" title="Equipamentos"><img src="imagens/produtos/equipamentos-01.jpg" alt="Equipamentos" title="Equipamentos"/></a>
            <h2><a href="<?=$url?>equipamentos-categoria" title="Equipamentos">Equipamentos</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>expositor-categoria" title="Expositor"><img src="imagens/produtos/expositor-01.jpg" alt="Expositor" title="Expositor"/></a>
            <h2><a href="<?=$url?>expositor-categoria" title="Expositor">Expositor</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>forno-categoria" title="Forno"><img src="imagens/produtos/forno-01.jpg" alt="Forno" title="Forno"/></a>
            <h2><a href="<?=$url?>forno-categoria" title="Forno">Forno</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>mobiliario-categoria" title="Mobiliário"><img src="imagens/produtos/mobiliario-01.jpg" alt="Mobiliário" title="Mobiliário"/></a>
            <h2><a href="<?=$url?>mobiliario-categoria" title="Mobiliário">Mobiliário</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>refrigeracao-categoria" title="Refrigereção"><img src="imagens/produtos/refrigeracao-01.jpg" alt="Refrigereção" title="Refrigereção"/></a>
            <h2><a href="<?=$url?>refrigeracao-categoria" title="Refrigereção">Refrigereção</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>servicos-categoria" title="Serviços"><img src="imagens/produtos/servicos-01.jpg" alt="Serviços" title="Serviços"/></a>
            <h2><a href="<?=$url?>servicos-categoria" title="Serviços">Serviços</a></h2>
          </li>
        </ul>
      </article>
    </div>
  </main>
  <? include('inc/form-mpi.php');?>
</div>
<? include('inc/footer.php');?>

</body>
</html>