<?php if ($url_relation != 0):
  $prodDownloads = array();
  $Read = new Read;
  $url_relation = explode(',', trim($url_relation, ','));
  foreach ($url_relation as $urlsr):
    $Read->ExeRead(TB_DOWNLOAD, "WHERE dow_id = :id AND user_empresa = :emp ORDER BY dow_title ASC", "id={$urlsr}&emp=" . EMPRESA_CLIENTE);
    if ($Read->getResult()):
      foreach ($Read->getResult() as $downs):
        array_push($prodDownloads, array("dow_title" => $downs['dow_title'], "dow_file" => $downs['dow_file']));
      endforeach;
    endif;
  endforeach;
endif; ?>
<div class="prod-inc-tab">
  <h2 class="tab__link active" onclick="openAba(event, 'desc')">Descrição</h2>
  <?php if(count($prodDownloads) > 0):?>
    <h2 class="tab__link" onclick="openAba(event, 'down')">Downloads</h2>
  <?php endif;?>
</div>
<div class="prod-inc-content">
  <div id="desc" class="tab__content" style="display: block;">
    <?php
      $prod_content = preg_replace('/(src=")(.*)(\/doutor\/uploads\/2\/)/mU', '$1'.RAIZ.'$3', $prod_content);
      $prod_content = str_replace('src="doutor', 'src="'.RAIZ.'/doutor', $prod_content);
      echo $prod_content;
    ?>
  </div>
  <?php if(count($prodDownloads) > 0):?>
    <div id="down" class="tab__content" style="display: none;">
      <p class="dark text-uppercase"><strong>Confira os downloads disponíveis para este produto:</strong></p>
      <?php foreach ($prodDownloads as $downloadItem): ?>
        <a href="<?= RAIZ . "/doutor/uploads/" . $downloadItem['dow_file']; ?>" title="<?= $downloadItem['dow_title']; ?>" class="btn" target="_blank"><i class="fas fa-file-pdf"></i> <?= $downloadItem['dow_title']; ?></a>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>