<footer>
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?=$nomeSite." - ".$slogan?></span>
			</address>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<li><a rel="nofollow" href="<?=$url?>" title="Página inicial">Home</a></li>
					<li><a rel="nofollow" href="<?=$url?>sobre-nos" title="Sobre nós">Sobre nós</a></li>
					<li><a rel="nofollow" href="<?=$url?>produtos" title="Produtos">Produtos</a></li>
					<li><a href="<?=$url?>mapa-site" title="Mapa do site">Mapa do site</a></li>
				</ul>
			</nav>
		</div>
		<br class="clear">
	</div>
</footer>
<div class="copyright-footer">
	<div class="wrapper-footer">
		<div class="copy">Copyright © <?=$nomeSite?>. (Lei 9610 de 19/02/1998)</div>
		<div class="center-footer">
			<img src="imagens/img-home/logo-footer.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>">
			<p>é um parceiro</p>
			<img src="imagens/img-home-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
		</div>
		<div class="selos">
			<a rel="noopener nofollow" href="http://validator.w3.org/check?uri=<?=$url.$urlPagina?>" target="_blank" title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
			<a rel="noopener nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$url.$urlPagina?>" target="_blank" title="CSS W3C" ><i class="fab fa-css3"></i> <strong>W3C</strong></a>
		</div>
	</div>
</div>
<script defer src="<?=$url?>js/geral.js"></script>

<!-- Shark Orcamento -->

<script>
var guardar = document.querySelectorAll('.botao-cotar');
for(var i = 0; i < guardar.length; i++){
  var adicionando = guardar[i].parentNode;
  adicionando.classList.add('nova-api');
};
</script>
<app-cotacao-solucs
appConfig='{"btnOrcamento": ".botao-cotar", "titulo": "h2", "industria": "solucoes-industriais", "container":"nova-api"}'
></app-cotacao-solucs>
<link rel="stylesheet" href="https://sdk.solucoesindustriais.com.br/dist/sdk-cotacao-solucs/styles.css">
<script src="https://sdk.solucoesindustriais.com.br/dist/sdk-cotacao-solucs/package-es5.js?v=<?= date_timestamp_get(date_create()); ?>" nomodule defer></script>



<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-MT3ZLBHMBJ"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-MT3ZLBHMBJ');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-D23WW3S4NC');
</script>

<!-- BOTAO SCROLL -->
<script async src="<?=$url?>js/jquery.scrollUp.min.js"></script>
<script async src="<?=$url?>js/scroll.js"></script>
<!-- /BOTAO SCROLL -->
<script async src="<?=$url?>js/vendor/modernizr-2.6.2.min.js"></script>
<script async src="<?=$url?>js/app.js"></script>


<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script><?php include 'inc/fancy.php'; ?>

<script type="module">
  import Typebot from 'https://cdn.jsdelivr.net/npm/@typebot.io/js@0.2.81/dist/web.js'

<<<<<<< Updated upstream
<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>
        <div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>
<script type="module">
import Typebot from 'https://cdn.jsdelivr.net/npm/@typebot.io/js@0.2.81/dist/web.js'

Typebot.initBubble({
typebot: "chatbotsatelites",
prefilledVariables: {
source: 'Supermercados Virtuais',
URL: 'https://supermercadosvirtuais.com.br', },
apiHost: "https://chat.ferramentademarketing.com.br",
previewMessage: {
message:
"Oi! Posso te ajudar?",
autoShowDelay: 1000,
avatarUrl:
"https://s3.typebot.io/public/workspaces/clzir1det0001bsmim0a73co8/typebots/clzir2kub0005bsmicrxd9r3c/hostAvatar?v=1722968705385",
},
theme: {
button: { backgroundColor: "#003ac2" },
previewMessage: {
backgroundColor: "#0042DA",
textColor: "#FFFFFF",
closeButtonBackgroundColor: "#0042DA",
closeButtonIconColor: "#FFFFFF",
},
chatWindow: { backgroundColor: "#fff" },
},
});
</script>
<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
