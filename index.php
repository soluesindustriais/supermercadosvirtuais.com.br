<?
$h1         = 'Supermercados Virtuais';
$title      = 'Página inicial';
$desc       = 'Supermercados Virtuais - Conte com os melhores fornecedores de equipamentos de supermercados do Brasil em um só lugar gratuitamente. Veja tudo sobre';
$var        = 'Home';
include('inc/head.php');
include('inc/fancy.php');
?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1>SUPERMERCADOS</h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>Caixa Checkout</h2>
        <p>Buscou por Caixa Checkout, você vai encontrar na plataforma Supermercados Virtuais, cote produtos agora mesmo com mais de 30 fornecedores ao mesmo tempo. É grátis!</p>
        <a  class="cd-btn btn-intro-ml-3 botao-cotar" id="orcamentoHome">Cotar agora mesmo!</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Carrinho de mercado</h2>
        <p>Se pesquisa por Carrinho de mercado, encontre as melhores indústrias, faça uma cotação hoje mesmo com mais de 200 fornecedores em um só lugar gratuitamente!</p>
        <a class="cd-btn btn-intro-ml-3 botao-cotar" id="orcamentoHome">Cotar agora mesmo!</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Expositor de bebidas</h2>
        <p>Compare preços de Expositor de bebidas, encontre os melhores fabricantes, realize um orçamento já com aproximadamente 100 distribuidores ao mesmo tempo gratuitamente!</p>
        <a class="cd-btn btn-intro-ml-3 botao-cotar" id="orcamentoHome">Cotar agora mesmo!</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <section class="wrapper-main">
    <div class="destaque txtcenter">
        <h2>Produtos <b>em destaque</b></h2>
      </div>
    <div class="products">
        <?php include('inc/produtos.php'); ?>
    </div>
    <div class="destaque txtcenter">
        <h2>Faça diversas <b>cotações</b></h2>
      </div>
      <div class="main-center">
      <div class=" quadro-2 ">
        <h2>Equipamentos para supermercado</h2>
        <div class="div-img">
          <p>Existem diversos fatores que determinam o sucesso ou o fracasso de um estabelecimento. Informação adequada, atendimento de qualidade, organização e disposição de produtos, preços e ofertas são alguns deles. Mas os equipamentos para supermercado também são essenciais para que os processos se desenrolem da melhor maneira possível. </p>
        </div>
        <div class="gerador-svg">
          <img src="imagens/img-home/construcao-civil.jpg" alt="Construção civil" title="Construção civil">
        </div>
      </div>
      <div class=" incomplete-box">
        <ul data-anime="in">
          <li>
            <p>Para ajudá-lo a conhecer alguns dos equipamentos mais importantes:</p>
            
            <li><i class="fas fa-angle-right"></i> Gôndolas</li>
            <li><i class="fas fa-angle-right"></i> Sacolas</li>
            <li><i class="fas fa-angle-right"></i> Check outs</li>
            <li><i class="fas fa-angle-right"></i> Balcão refrigerado</li>
            <li><i class="fas fa-angle-right"></i> Fogão industrial</li>
          </ul>
          <a href="<?=$url?>equipamentos-para-padaria" class="btn-4">Saiba mais</a>
        </div>
      </div>
      <br>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
      <div class="content-icons">
        <div class="produtos-relacionados-1">
          <figure>
            <a href="<?=$url?>forno-pizza">
              <div class="fig-img">
                <h2>Forno pizza</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-2">
          <figure class="figure2">
            <a href="<?=$url?>freezer-expositor">
              <div class="fig-img2">
                <h2>Freezer expositor</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-3">
          <figure>
            <a href="<?=$url?>caixa-de-supermercado">
              <div class="fig-img">
                <h2>Caixa de supermercado</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/refrigeracao-01.jpg" class="lightbox" title="Refrigeração">
              <img src="<?=$url?>imagens/img-home/thumbs/refrigeracao-01.jpg" title="Refrigeração" alt="Refrigeração">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/carrinhos-01.jpg" class="lightbox"  title="Carrinhos de supermercado">
              <img src="<?=$url?>imagens/img-home/thumbs/carrinhos-01.jpg" alt="Carrinhos de supermercado" title="Carrinhos de supermercado">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/equipamentos-01.jpg" class="lightbox" title="Equipamentos para supermercado">
              <img src="<?=$url?>imagens/img-home/thumbs/equipamentos-01.jpg" alt="Equipamentos para supermercado" title="Equipamentos para supermercado">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/expositor-01.jpg" class="lightbox" title="Expositor">
              <img src="<?=$url?>imagens/img-home/thumbs/expositor-01.jpg" alt="Expositor" title="Expositor">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/forno-01.jpg" class="lightbox" title="Forno">
              <img src="<?=$url?>imagens/img-home/thumbs/forno-01.jpg" alt="Forno"  title="Forno">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/mobiliario-01.jpg" class="lightbox" title="Mobiliária para mercado">
              <img src="<?=$url?>imagens/img-home/thumbs/mobiliario-01.jpg" alt="Mobiliária para mercado" title="Mobiliária para mercado">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/refrigeracao-02.jpg" class="lightbox" title="Refrigerados">
              <img src="<?=$url?>imagens/img-home/thumbs/refrigeracao-02.jpg" alt="Refrigerados" title="Refrigerados">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/servicos-01.jpg" class="lightbox" title="Serviços">
              <img src="<?=$url?>imagens/img-home/thumbs/servicos-01.jpg" alt="Serviços" title="Serviços">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/equipamentos-02.jpg" class="lightbox" title="Equipamentos supermercado">
              <img src="<?=$url?>imagens/img-home/thumbs/equipamentos-02.jpg" alt="Equipamentos supermercado" title="Equipamentos supermercado">
            </a>
            </li>
            <li><a href="<?=$url?>imagens/img-home/mobiliario-02.jpg" class="lightbox" title="Móveis para supermercado">
              <img src="<?=$url?>imagens/img-home/thumbs/mobiliario-02.jpg" alt="Móveis para supermercado" title="Móveis para supermercado">
            </a>
            </li>
          </ul>
        </div>
      </div>
</section>
<? include('inc/form-mpi.php'); ?>
</main>
<? include('inc/footer.php'); ?>
<link rel="stylesheet" href="<?=$url?>nivo/nivo-slider.css" media="screen">
<script  src="<?=$url?>nivo/jquery.nivo.slider.js"></script>
<script >
$(window).load(function() {
$('#slider').nivoSlider();
});
</script>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
<script type="text/javascript" src="slick/slick.min.js"></script>
<script>
$('.products').slick({
dots: true,
infinite: true,
speed: 300,
autoplay: true,
slidesToShow: 4,
slidesToScroll: 4,
responsive: [
{
breakpoint: 1024,
settings: {
slidesToShow: 3,
slidesToScroll: 3,
infinite: true,
dots: true
}
},
{
breakpoint: 600,
settings: {
slidesToShow: 2,
slidesToScroll: 2
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}
// You can unslick at a given breakpoint now by adding:
// settings: "unslick"
// instead of a settings object
]
});
</script>
</body>
</html>