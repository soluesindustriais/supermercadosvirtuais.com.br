User-agent: *
Disallow: /inc/
Disallow: /imagens/carrinhos/thumbs/
Disallow: /imagens/equipamentos/thumbs/
Disallow: /imagens/expositor/thumbs/
Disallow: /imagens/forno/thumbs/
Disallow: /imagens/mobiliario/thumbs/
Disallow: /imagens/refrigeracao/thumbs/
Disallow: /imagens/servicos/thumbs/

Sitemap: https://www.supermercadosvirtuais.com.br/sitemap.xml
